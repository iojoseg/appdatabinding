package mx.tecnm.misantla.appaboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_main.*
import mx.tecnm.misantla.appaboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    //creamos
    private val myName:MyName = MyName("Juan Perez")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
      binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        binding.myName =myName
       /*button.setOnClickListener {
           addNickName(it)
       }*/
        binding.button.setOnClickListener {
            addNickName(it)
        }
    }

    private fun  addNickName(view: View) {
       /*
        txtNickName.text = EdtNickName.text
        EdtNickName.visibility = View.GONE
        view.visibility = View.GONE
        txtNickName.visibility = View.VISIBLE
*/
        binding.apply {
       // txtNickName.text = binding.EdtNickName.text
            myName?.nickname = EdtNickName.text.toString()
            invalidateAll()
        EdtNickName.visibility = View.GONE
        button.visibility = View.GONE
        txtNickName.visibility = View.VISIBLE
          //  binding.txtNickName.visibility = View.VISIBLE

        }
        //hide the keyboard
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken,0)
    }
}